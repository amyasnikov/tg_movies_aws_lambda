fmt:
	gofmt -w -s -d .

depends:
	go mod tidy

clean:
	rm -f main 
	rm -f main.zip

main:
	GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o main *.go

zip:
	zip main.zip main

test:
	go clean -testcache
	go test -v .
