package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	tb "gopkg.in/tucnak/telebot.v2"
)

func HandlerTrace(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	data, err := json.MarshalIndent(request, "", "  ")
	if err != nil {
		fmt.Printf("Body: %s \n", err.Error())
		return events.APIGatewayProxyResponse{
			Body:       err.Error(),
			StatusCode: 500,
		}, nil
	}

	fmt.Printf("Body: %s \n", string(data))

	return events.APIGatewayProxyResponse{
		Body:       string(data),
		StatusCode: 200,
	}, nil
}

func HandlerTg(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Printf("Body: %s \n", request.Body)

	token := os.Getenv("TGTOKEN")
	if token == "" {
		return events.APIGatewayProxyResponse{StatusCode: 200, Body: `{"ok":false,"error_message":"empty TGTOKEN"}`}, nil
	}

	update := tb.Update{}
	if err := json.Unmarshal([]byte(request.Body), &update); err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 200, Body: `{"ok":false,"error_message":"can not parse body"}`}, nil
	}

	if update.Message == nil {
		return events.APIGatewayProxyResponse{StatusCode: 200, Body: `{"ok":false,"error_message":"empty messages"}`}, nil
	}

	bot, err := tb.NewBot(tb.Settings{Token: token})
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 200, Body: `{"ok":false,"error_message":"can not create bot"}`}, nil
	}

	message := update.Message

	if message.Text == "/start" || message.Text == "Next Question" {
		if SendQuiz(bot, message) == false {
			bot.Send(message.Chat, "Can not send quiz")
		}
	} else {
		text := "Hi, " + message.Chat.FirstName + ". "
		text += "Please, send /start to start quiz. "
		text += "You sent message: " + message.Text
		bot.Send(message.Chat, text)
	}

	return events.APIGatewayProxyResponse{StatusCode: 200, Body: `{"ok":true}`}, nil
}

func SendQuiz(bot *tb.Bot, message *tb.Message) bool {
	menuQuiz := &tb.ReplyMarkup{ResizeReplyKeyboard: true}
	btnQuizNext := menuQuiz.Text("Next Question")

	menuQuiz.Reply(
		menuQuiz.Row(btnQuizNext),
	)

	quiz, err := GetQuiz()
	if err != nil {
		return false
	}

	p := &tb.Photo{File: tb.FromURL(quiz.Photo)}
	_, err = bot.SendAlbum(message.Chat, tb.Album{p}, menuQuiz)
	if err != nil {
		return false
	}

	poll := &tb.Poll{
		Type:          tb.PollQuiz,
		Question:      quiz.Question,
		CorrectOption: quiz.CorrectId,
	}

	for _, o := range quiz.Options {
		poll.AddOptions(o)
	}

	_, err = bot.Send(message.Chat, poll, menuQuiz)
	if err != nil {
		return false
	}

	return true
}

func main() {
	lambda.Start(HandlerTg)
}
