package main

import (
	"errors"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"time"
)

type Quiz struct {
	Question  string   `json:"question"`
	Photo     string   `json:"photo"`
	Options   []string `json:"options"`
	CorrectId int      `json:"correct_option_id"`
}

func GetQuiz() (Quiz, error) {
	rand.Seed(time.Now().UTC().UnixNano())

	quiz := Quiz{}

	if len(MoviesV) == 0 {
		return quiz, errors.New("empty Movies")
	}

	movie := MoviesV[rand.Intn(len(MoviesV))]

	quiz.Question = "What is name of a movie?"
	quiz.Photo = movie.Photos[rand.Intn(len(movie.Photos))]

	similarCount := 3
	optionsCount := 7

	if val := os.Getenv("SIMILAR_COUNT"); val != "" {
		similarCount, _ = strconv.Atoi(val)
	}

	if val := os.Getenv("OPTIONS_COUNT"); val != "" {
		optionsCount, _ = strconv.Atoi(val)
	}

	options := make(map[string]bool)
	options[movie.Name] = true

	if len(movie.MoreLikeNames) > 0 {
		for i := 0; i < similarCount; i++ {
			name := movie.MoreLikeNames[rand.Intn(len(movie.MoreLikeNames))]
			options[name] = true
		}
	}

	for i := 0; i < optionsCount && len(options) < optionsCount; i++ {
		options[MoviesV[rand.Intn(len(MoviesV))].Name] = true
	}

	quiz.Options = make([]string, 0, optionsCount)
	for name := range options {
		quiz.Options = append(quiz.Options, name)
	}

	sort.Strings(quiz.Options)
	for ind, name := range quiz.Options {
		if name == movie.Name {
			quiz.CorrectId = ind
		}
	}

	return quiz, nil
}
