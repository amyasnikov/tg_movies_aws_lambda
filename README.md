# TG_MOVIES_AWS_LAMBDA

* Author: Alexander Myasnikov
* mailto: myasnikov.alexander.s@gmail.com
* git: https://gitlab.com/amyasnikov/tg_movies_aws_lambda



## Description

* telegram bot @amyasnikov_movies_bot



## HOWTO

* Creating lambda function
  * Build main.zip, for example ```make depends && make clean && make main && make test && make zip```
  * Upload code from .zip file, for example ```aws lambda update-function-code --function-name=<name> --zip-file=fileb://./main.zip```
  * Specify "Handler: main" in runtime settings
  * Specify env TGTOKEN in format: XXXXXXXXXX:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* Creating Gateway API
  * Specify integration point as lambda in resource with POST method
  * Set flag "Use Lambda Proxy integration"
  * Deploy API
* Setting webhook
  * Run ```https://api.telegram.org/bot<TOKEN>/setWebhook?url=<URL>```
  * Run ```curl -X POST -F "url=<URL>" "https://api.telegram.org/bot<TOKEN>/setWebhook"```
* Tesning
  * Send any message to telegram bot




